FROM docker.io/rust:slim

WORKDIR /usr/src/meme-reposter
COPY . .
RUN cargo install --path .
ENTRYPOINT [ "meme-reposter" ]
